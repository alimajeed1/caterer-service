# React.js CRUD App with React Router & Axios

Build a React.js CRUD Application to consume Web API, display and modify data with Router, Axios & Bootstrap.

React Caterers Application in that:
- Each Caterer has id, name, address, capacity and contact.
- We can create, retrieve by id, name or city.
- There is a Search bar for finding caterers by name or city.
- We can also add a caterer

Screens Placed in the folder for reference

![Screen1](Screen1.png)
![Screen2](Screen2.png)
![Screen3](Screen3.png)
![Screen4](Screen4.png)

### Set port
.env
```
PORT=8082
```

## Project setup

In the project directory, you can run:

```
npm install
# or
yarn install
```

or

### Compiles and hot-reloads for development

```
npm start
# or
yarn start
```

Open [http://localhost:8082](http://localhost:8082) to view it in the browser.

The page will reload if you make edits.

### Configs
http-commons contains link to base url of api

### Steps
You can search preloaded caterers by their name or city
e.g Search By City -> 'Stockholm'
	Search By Name -> 'Muhammad Bishop'
	
You can also add new caterer putting data in the form.